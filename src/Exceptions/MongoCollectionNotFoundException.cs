﻿using System;
using MongoDB.Driver;

namespace ProSkive.Lib.MongoDb.Exceptions
{
    public class MongoCollectionNotFoundException : MongoException
    {
        public MongoCollectionNotFoundException(string collectionName) 
            : base($"The configured mongo collection ('{collectionName}') could not be found / retrieved")
        {
        }

        public MongoCollectionNotFoundException(string collectionName, Exception innerException) 
            : base($"The configured mongo collection ('{collectionName}') could not be found / retrieved", innerException)
        {
        }
    }
}
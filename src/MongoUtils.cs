﻿using System;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using ProSkive.Lib.MongoDb.Settings;

namespace ProSkive.Lib.MongoDb
{
    public static class MongoUtils
    {
        public static bool ConnectToCollection<T>(MongoDbSettings mongoSettings, out IMongoCollection<T> collection, out IMongoClient client, out IMongoDatabase database)
        {
            // Url
            var url = new MongoUrl(mongoSettings.ConnectionString);
            
            // Settings
            var mongoClientSettings = MongoClientSettings.FromUrl(url);
            mongoClientSettings.ConnectTimeout = TimeSpan.FromSeconds(10);

            if (mongoSettings.Auth.UseAuth)
            {
                mongoClientSettings.Credential = MongoCredential.CreateCredential(
                    mongoSettings.Auth.AuthDatabase, 
                    mongoSettings.Auth.Username, 
                    mongoSettings.Auth.Password);           
            }
            
            // Create Mongo Client
            client = new MongoClient(mongoClientSettings);
                
            // Get Database
            database = client.GetDatabase(mongoSettings.Database);
                
            // Prepare collection
            collection = null;
            
            // Check connection
            var isMongoLive = database.RunCommandAsync((Command<BsonDocument>) "{ping:1}").Wait(3000);
            if (!isMongoLive)
                return false;

            collection = database.GetCollection<T>(mongoSettings.CollectionName);
            
            return true;
        }
        
        public static bool ConnectToCollection<T>(MongoDbSettings mongoSettings, out IMongoCollection<T> collection, out IMongoClient client)
        {
            return ConnectToCollection<T>(mongoSettings, out collection, out client, out _);
        }
        
        public static bool ConnectToCollection<T>(MongoDbSettings mongoSettings, out IMongoCollection<T> collection)
        {
            return ConnectToCollection<T>(mongoSettings, out collection, out _, out _);
        }

        public static async Task<bool> IsCollectionEmptyAsync<T>(IMongoCollection<T> collection)
        {
            var count = await collection.CountAsync(new BsonDocument());
            return count < 1;
        }
    }
}
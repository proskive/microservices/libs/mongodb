namespace ProSkive.Lib.MongoDb.Settings.Snapshots
{
    public class MongoOptionsSnapshot
    {
        public MongoDbSettings MongoDb { get; set; }
    }
}
﻿namespace ProSkive.Lib.MongoDb.Settings
{
    public class MongoAuth
    {
        public bool UseAuth { get; set; }
        public string AuthDatabase { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
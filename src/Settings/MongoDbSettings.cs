namespace ProSkive.Lib.MongoDb.Settings
{
    public class MongoDbSettings
    {
        public string ConnectionString { get; set; }
        public string Database { get; set; }
        public string CollectionName { get; set; }
        public MongoAuth Auth { get; set; }
    }
}
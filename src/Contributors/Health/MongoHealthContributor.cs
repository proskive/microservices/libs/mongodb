﻿using Microsoft.Extensions.Options;
using ProSkive.Lib.MongoDb.Settings.Snapshots;
using Steeltoe.Management.Endpoint.Health;

namespace ProSkive.Lib.MongoDb.Contributors.Health
{
    public class MongoHealthContributor : IHealthContributor
    {
        private readonly MongoOptionsSnapshot settings;
        public string Id { get; } = "db";

        public MongoHealthContributor(IOptionsSnapshot<MongoOptionsSnapshot> settings)
        {
            this.settings = settings.Value;
        }
        
        public Steeltoe.Management.Endpoint.Health.Health Health()
        {
            var result = new Steeltoe.Management.Endpoint.Health.Health();
            result.Details.Add("database", settings.MongoDb.Database);
            result.Details.Add("collection", settings.MongoDb.CollectionName);

            var status = CheckHealth();
            result.Details.Add("status", status.ToString());
            result.Status = status;

            return result;
        }

        private HealthStatus CheckHealth()
        {
            var connectionEstablished = MongoUtils.ConnectToCollection<object>(
                settings.MongoDb,
                out _
            );

            return !connectionEstablished ? HealthStatus.OUT_OF_SERVICE : HealthStatus.UP;
        }
    }
}
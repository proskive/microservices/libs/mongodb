﻿using MongoDB.Driver;
using ProSkive.Lib.MongoDb.Settings;

namespace ProSkive.Lib.MongoDb.Helpers
{
    public class MongoTestHelper<TModel>
    {
        private readonly MongoDbSettings mongoSettings;
        private readonly IMongoDatabase database;
        private IMongoCollection<TModel> collection;

        public MongoTestHelper(MongoDbSettings mongoSettings)
        {
            this.mongoSettings = mongoSettings;
            
            // Establish connection
            MongoUtils.ConnectToCollection<TModel>(mongoSettings, out collection, out _, out database);
        }
        
        public void CleanCollection()
        {
            database.DropCollection(mongoSettings.CollectionName);
            database.CreateCollection(mongoSettings.CollectionName);
            collection = database.GetCollection<TModel>(mongoSettings.CollectionName);
        }

        public long CountDocuments()
        {
            return collection.Count(_ => true);
        }
    }
}
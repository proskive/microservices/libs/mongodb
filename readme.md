# ProSkive.Lib.MongoDb

.NET Standard 2.0 Library for MongoDB support.

## Config

```json
"MongoDb": {
    "ConnectionString": "mongodb://localhost:27017/",
    "Database": "dbname",
    "NotificationCollectionName": "collectionname",
    "Auth": {
        "UseAuth": true,
        "AuthDatabase": "admin",
        "Username": "username",
        "Password": "password"
    }
},
```

## Startup

```c#
public void ConfigureServices(IServiceCollection services)
{
    // ...
    
    services.Configure<MongoOptionsSnapshot>(Configuration);
    
    // When using Health endpoints from Steeltoe
    services.AddScoped<IHealthContributor, MongoHealthContributor>();
    
    // ...
}
```

## License GPL2

```
ProSkive
Copyright (C) 2018 Medical Informatics Group (MIG) Frankfurt

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
USA.
```
